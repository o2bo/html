package com.example.loseweight;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.JsonReader;

public class JsonHandle {
	static final int URL_CONNECTION_TIMEOUT = 8000;
	static final int URL_CONNECTION_READ_TIMEOUT = 30000;
	
	//获取json字符串
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	//下载并保存json文件
	public static void requestAndSave(String url, String filename, Context context) {
		HttpClient httpclient = new DefaultHttpClient();
		final HttpParams http_params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(http_params, URL_CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(http_params, URL_CONNECTION_READ_TIMEOUT);

		// Prepare a request object
		HttpGet httpget = new HttpGet(url); 

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {
				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				FileOutputStream ofile = context.openFileOutput(filename, 0);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = instream.read(buffer)) != -1) {
					ofile.write(buffer, 0, len);
				}
				instream.close();
				ofile.close();
			}


		} catch (Exception e) {
		}
	}
	
}
