package com.example.loseweight;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;




import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Video;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.widget.ViewSwitcher.ViewFactory;

public class MainActivity extends Activity
{
	static final int URL_CONNECTION_TIMEOUT = 8000;
	static final int URL_CONNECTION_READ_TIMEOUT = 30000;
	public String jsonData;
	private Context mContext;
	DisplayImageOptions options;
	public static ImageButton right;
	public static ImageButton left;
	public static GridView grid;

	// 定义一个常量，用于显示每屏显示的应用程序数
	public static final int NUMBER_PER_SCREEN = 8;

	// 代表应用程序的内部类，
	public static class DataItem
	{
		// 模块名称
		public String dataName;
		public String name;
		public String url;
		public String pic;
		public String type;
	}




	// 保存系统所有应用程序的List集合
	private ArrayList<DataItem> items = new ArrayList<DataItem>();
	// 记录当前正在显示第几屏的程序 
	public static int screenNo = -1;
	// 保存程序所占的总屏数
	private int screenCount;
	public static ViewSwitcher switcher;
	// 创建LayoutInflater对象
	LayoutInflater inflater;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MobclickAgent.setDebugMode( true );
		UmengUpdateAgent.setUpdateOnlyWifi(false);
		UmengUpdateAgent.update(this);
		UmengUpdateAgent.setUpdateAutoPopup(true);
		UmengUpdateAgent.setUpdateListener(null);
		UmengUpdateAgent.setDownloadListener(null);
		UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
			@Override
			public void onUpdateReturned(int updateStatus,UpdateResponse updateInfo) {
				switch (updateStatus) {
				case 0: // has update
					UmengUpdateAgent.showUpdateDialog(mContext, updateInfo);
					break;
				case 1: // has no update
					break;
				case 2: // none wifi
					break;
				case 3: // time out
					break;
				}
			}
		});
		right = (ImageButton) findViewById(R.id.button_next);
		left = (ImageButton) findViewById(R.id.button_prev);
		switcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
		jsonData = GetJson();
		options = new DisplayImageOptions.Builder()
		// 在显示真正的图片前，会加载这个资源
		.showStubImage(R.drawable.p) 
		//空的Url时显示这个资源
		.showImageForEmptyUri(R.drawable.p) 
		//加载/解码出现错误时，显示这个资源
		.showImageOnFail(R.drawable.p) 
		//设置下载的图片是否缓存在内存中
		.cacheInMemory(true)
		//设置下载的图片是否缓存在SD卡中
		.cacheOnDisc(false)
		//设置图片的解码类型
		.bitmapConfig(Bitmap.Config.ARGB_8888)
		//.displayer(new SimpleBitmapDisplayer())
		.build();
		json(jsonData);
		right.setOnFocusChangeListener(new View.OnFocusChangeListener() {        
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					next(right);
				}else{
					//showTextView.setText("组件失去了焦点");
				}
			}
		});
		left.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){

					prev(left);
				}else{

				}
			}        

		});

		inflater = LayoutInflater.from(MainActivity.this);

		// 计算应用程序所占的总屏数。
		// 如果应用程序的数量能整除NUMBER_PER_SCREEN，除法的结果就是总屏数。
		// 如果不能整除，总屏数应该是除法的结果再加1。
		screenCount = items.size() % NUMBER_PER_SCREEN == 0 ? 
				items.size()/ NUMBER_PER_SCREEN :
					items.size() / NUMBER_PER_SCREEN	+ 1;
				switcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
				switcher.setFactory(new ViewFactory()
				{
					// 实际上就是返回一个GridView组件
					@Override
					public View makeView()
					{
						// 加载R.layout.slidelistview组件，实际上就是一个GridView组件。
						View view = inflater.inflate(R.layout.slidelistview, null);
						grid = (GridView)view.findViewById(R.id.gridview);
						grid.setAdapter(adapter);

						grid.setOnItemClickListener(new OnItemClickListener() {
							public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
								String url = items.get(screenNo * NUMBER_PER_SCREEN + position).url;
								String type = items.get(screenNo * NUMBER_PER_SCREEN + position).type;
								String M3u8Url=GetM3u8Url(url,type);
								//使用视频应用播放
//								Intent intent = new Intent(Intent.ACTION_VIEW);
//								String type_2 = "video/mp4";
//								Uri name = Uri.parse(M3u8Url);
//								intent.setDataAndType(name, type_2);
//								startActivity(intent);
								//使用videoview播放
								Intent intent = new Intent();  
								intent.setClass(MainActivity.this, VideoPlay.class);  
								Bundle bundle = new Bundle();  
								bundle.putString("url", M3u8Url);   
								intent.putExtras(bundle); 
								startActivity(intent);  
							}
						});
						return view;
					}
				});

				//将左侧按钮清除
				screenJudge();
				// 页面加载时先显示第一屏。
				next(null);
				new AppListSyncer().execute("");

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	public String GetM3u8Url(String url,String type)
	{

		String m3u8url_a = "http://v.youku.com/player/getRealM3U8/vid/";
		String m3u8url_b = "/type/";
		String m3u8url_c = "/ts/";
		Date date = new Date();
		long time = date.getTime();
		String timestamp = time + "";
		timestamp = timestamp.substring(0, 10);
		String m3u8url_d = "/useKeyframe/0/video.m3u8";
		String m3u8url=m3u8url_a+url+m3u8url_b+type+m3u8url_c+timestamp+m3u8url_d;
		return m3u8url;
	}



	private class AppListSyncer extends AsyncTask<String, Integer, Integer> {

		@Override
		protected Integer doInBackground(String... params) {
			JsonHandle.requestAndSave("http://static.wukongtv.com/json/zhengduoyan/list.json", "list.json", MainActivity.this);
			return null;
		}

	}


	//获取assets中json文件
	public String GetJson()
	{
		String json = "";
		String result = null;
		boolean found = true;
		// find in private files dir
		try {
			FileInputStream ins = MainActivity.this.openFileInput("list.json");
			result = JsonHandle.convertStreamToString(ins);
			json = result;
		}
		catch (Exception e) {
			found = false;
		}
		//在assets文件夹中寻找json文件
		if(!found){
			json = "";
			StringBuilder stringBuilder = new StringBuilder();
			try {
				BufferedReader bf = new BufferedReader(new InputStreamReader(
						getAssets().open("list.json")));
				String line;
				while ((line = bf.readLine()) != null) {
					stringBuilder.append(line);
				}
				json = stringBuilder.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	//解析json文件
	@SuppressLint("NewApi") public void json(String v)
	{
		try 
		{
			JsonReader jsonReader=new JsonReader(new StringReader(jsonData));
			//开始读取json数据
			jsonReader.beginArray();
			//循环读取json中的数据，一直到读取数据结束
			int i = 0;
			while(jsonReader.hasNext())
			{
				//开始读取json对象
				jsonReader.beginObject();
				//循环读取json对象中的数据，一直到读取数据结束
				String name = "";
				String pic = "";
				String url = "";
				String type = "";
				while(jsonReader.hasNext())
				{
					//jsonReader.nextName()读取json对象的名称
					//jsonReader.nextString()读取json对象对应的值；如果是整形对象使用jsonReader.nextInt()方法
					if(jsonReader.nextName().equals("name"))
					{
						name = jsonReader.nextString();
					}
					if(jsonReader.nextName().equals("pic"))
					{
						pic = jsonReader.nextString();
					}
					if(jsonReader.nextName().equals("url"))
					{
						url = jsonReader.nextString();
					}
					if(jsonReader.nextName().equals("type"))
					{
						type = jsonReader.nextString();
					}
				}
				//System.out.println();
				//结束json对象数据的解析
				jsonReader.endObject();
				++i;
				//根据循环次数生成选项
				String label = "" + i;
				DataItem item = new DataItem();
				item.dataName = label;
				item.name = name;
				item.pic = pic;				
				item.url = url;
				item.type = type;
				items.add(item);
			}
			//结束json数据的解析
			jsonReader.endArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	//根据不同情况隐藏/显示左右翻页按钮
	public void screenJudge()
	{
		if(screenCount == 1)
		{
			left.setVisibility(View.INVISIBLE);
			right.setVisibility(View.INVISIBLE);
		}
		else if(screenCount == 2)
		{
			if(screenNo == 0)
			{
				left.setVisibility(View.INVISIBLE);
				right.setVisibility(View.VISIBLE);
			}
			else if(screenNo == screenCount-1)
			{
				right.setVisibility(View.INVISIBLE);
				left.setVisibility(View.VISIBLE);
			}
		}
		else
		{
			if(screenNo == 0)
			{
				//当前为第一屏
				left.setVisibility(View.INVISIBLE);
			}
			else if(screenNo == screenCount-1)
			{
				//当前为最后一屏
				right.setVisibility(View.INVISIBLE);
			}
			else
			{
				left.setVisibility(View.VISIBLE);
				right.setVisibility(View.VISIBLE);
			}
		}
	}

	public void next(View v)
	{
		if (screenNo < screenCount - 1)
		{
			screenNo++;
			// 为ViewSwitcher的组件显示过程设置动画
			switcher.setInAnimation(this, R.anim.slide_in_right);
			// 为ViewSwitcher的组件隐藏过程设置动画
			switcher.setOutAnimation(this, R.anim.slide_out_left);
			// 控制下一屏将要显示的GridView对应的 Adapter
			((GridView) switcher.getNextView()).setAdapter(adapter);
			// 点击右边按钮，显示下一屏，
			// 学习手势检测后，也可通过手势检测实现显示下一屏.
			switcher.showNext();  // ①
			switcher.requestFocus();
			screenJudge();
		}
	}

	public void prev(View v)
	{
		if (screenNo > 0)
		{
			screenNo--;
			// 为ViewSwitcher的组件显示过程设置动画
			switcher.setInAnimation(this, R.anim.slide_in_left);
			// 为ViewSwitcher的组件隐藏过程设置动画
			switcher.setOutAnimation(this, R.anim.slide_out_right);
			// 控制下一屏将要显示的GridView对应的 Adapter
			((GridView) switcher.getNextView()).setAdapter(adapter);
			// 点击左边按钮，显示上一屏，当然可以采用手势
			// 学习手势检测后，也可通过手势检测实现显示上一屏.
			switcher.showPrevious();   // ②
			switcher.requestFocus();
			screenJudge();
		}
	}

	// 该BaseAdapter负责为每屏显示的GridView提供列表项
	private BaseAdapter adapter = new BaseAdapter()
	{
		@Override
		public int getCount()
		{
			// 如果已经到了最后一屏，且应用程序的数量不能整除NUMBER_PER_SCREEN
			if (screenNo == screenCount - 1
					&& items.size() % NUMBER_PER_SCREEN != 0)
			{
				// 最后一屏显示的程序数为应用程序的数量对NUMBER_PER_SCREEN求余
				return items.size() % NUMBER_PER_SCREEN;
			}
			// 否则每屏显示的程序数量为NUMBER_PER_SCREEN
			return NUMBER_PER_SCREEN;
		}

		@Override
		public DataItem getItem(int position)
		{
			// 根据screenNo计算第position个列表项的数据
			return items.get(screenNo * NUMBER_PER_SCREEN + position);
		}

		@Override
		public long getItemId(int position)
		{
			return position;
		}
		//protected ImageLoader imageLoader = ImageLoader.getInstance();
		@Override
		public View getView(int position
				, View convertView, ViewGroup parent)
		{
			//复用已经生成好的view
			View view = convertView;
			if (convertView == null)
			{
				// 加载R.layout.labelicon布局文件
				view = inflater.inflate(R.layout.labelicon, null);
			}
			// 获取R.layout.labelicon布局文件中的ImageView组件，并为之设置图标
			ImageView imageView = (ImageView)
					view.findViewById(R.id.imageview);
			//设置图片
			String a = getItem(position).pic;
			ImageLoader.getInstance().displayImage(a, imageView, options);
			// 获取R.layout.labelicon布局文件中的TextView组件，并为之设置文本
			TextView textView = (TextView) 
					view.findViewById(R.id.textview);
			//设置文本
			String b = getItem(position).name;
			textView.setText("第"+getItem(position).dataName+"集 "+b);
			return view;
		}
	};

}