package com.example.loseweight;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlay extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.videoview); 
		Bundle bundle = this.getIntent().getExtras();  
		String url = bundle.getString("url");  
		VideoView video = (VideoView) findViewById(R.id.video); 
		Uri uri = Uri.parse(url);    
		video.setMediaController(new MediaController(this));    
		video.setVideoURI(uri);
		video.requestFocus();
		video.start();

	}  

}


