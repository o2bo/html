package com.wukongtv.app8tv;

public class Constants {
	public static final String APP_LIST_URL  = "http://static.wukongtv.com/json/applist.json";
	public static final String APP_LIST_FILENAME = "applist.json";
	
	// statistics constants
	public static final int EVENT_STARTDOWNLOAD = R.string.E_StartDownload;
	public static final int EVENT_OPEN = R.string.E_Open;
	public static final int EVENT_DOWNLOADING_DURATION = R.string.E_DownloadingDuration;
	public static final int EVENT_DOWNLOAD_FAILURE = R.string.E_DownloadFailure;
	public static final int EVENT_INSTALL_BEGIN = R.string.E_InstallBegin;
	public static final int EVENT_BACK_PRESSED = R.string.E_BackMenuAppeared;
	public static final int EVENT_SYSTEM_SETTING = R.string.E_SystemSetting;
	public static final int EVENT_MEMORY_CLEAN = R.string.E_MemoryClean;
}
