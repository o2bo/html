package com.wukongtv.app8tv;

import java.io.File;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.FIFOLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.wukongtv.app8tv.base.OmniStorage;

public class MyApplication extends Application {

	private OmniStorage mStorage;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		mStorage = new OmniStorage(getApplicationContext());
		mStorage.init();
		
		File cacheDir = mStorage.getCacheDir();
		Log.v("zhangge", cacheDir.getAbsolutePath());
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
		.taskExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
		.taskExecutorForCachedImages(AsyncTask.THREAD_POOL_EXECUTOR)
		.denyCacheImageMultipleSizesInMemory()
		.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
        .discCache(new UnlimitedDiscCache(cacheDir)) // default
        .memoryCache(new FIFOLimitedMemoryCache(2 * 1024 * 1024))
		.build();
		ImageLoader.getInstance().init(config);
	}
	
	public OmniStorage getOmniStorage() {
		return mStorage;
	}
}
