package com.wukongtv.app8tv;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.wukongtv.app8tv.base.AppList;
import com.wukongtv.app8tv.base.AppList.AppItem;

public class AppGridAdapter extends BaseAdapter implements ImageLoadingListener{
	
	private AppList mAppList;
	private MainActivity mContext;
	
	private DisplayImageOptions options;
	
	public static class ViewHolder {
		public final ImageView image;
		public final TextView text;
		public final ProgressBar pb;
		public final ImageView tag;
		public final FrameLayout frame;
		
		public ViewHolder(ImageView i, TextView t, ProgressBar p, ImageView tag, FrameLayout f) {
			image = i;
			text = t;
			pb = p;
			this.tag = tag;
			frame = f;
		}
	}
	
	public AppGridAdapter(MainActivity c) {
		mAppList = new AppList();
		mContext = c;
		BitmapFactory.Options decodingOptions = new BitmapFactory.Options();
		decodingOptions.inScaled = true;
		decodingOptions.inPurgeable = true;
		decodingOptions.inDensity = DisplayMetrics.DENSITY_HIGH;
		decodingOptions.inTargetDensity = mContext.getResources().getDisplayMetrics().densityDpi;
		//decodingOptions.inDither = true;
		options = new DisplayImageOptions.Builder()
		.bitmapConfig(Bitmap.Config.ARGB_8888)
		.decodingOptions(decodingOptions)
		.delayBeforeLoading(100)
		.cacheOnDisc(true)
		.cacheInMemory(true)
		.showStubImage(R.drawable.placeholder)
		//.displayer(new FadeInBitmapDisplayer(300))
		.build();
	}

	@Override
	public int getCount() {
		return mAppList.getCount();
	}
 
	@Override
	public Object getItem(int position) {
		return mAppList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView image;
		TextView text ;
		ProgressBar pb;
		ImageView tag;
		FrameLayout frame;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.app_item, parent, false);
			image = (ImageView) convertView.findViewById(R.id.app_icon);
			text = (TextView) convertView.findViewById(R.id.app_name);
			pb = (ProgressBar) convertView.findViewById(R.id.app_progress);
			tag = (ImageView) convertView.findViewById(R.id.app_not_installed_tag);
			frame = (FrameLayout) convertView.findViewById(R.id.app_icon_frame);
			convertView.setTag(new ViewHolder(image, text, pb, tag, frame));
		}
		else {
			ViewHolder vh = (ViewHolder) convertView.getTag();
			image = vh.image;
			text = vh.text;
			pb = vh.pb;
			tag = vh.tag;
		}
		
		AppItem item = (AppItem) getItem(position);
		
		if (item.status == AppList.STATUS_INSTALLED) {
			//tag.setVisibility(View.INVISIBLE);
			pb.setVisibility(View.INVISIBLE);
		}
		else {
			pb.setVisibility(View.VISIBLE);
			//tag.setVisibility(View.VISIBLE);
		}
		text.setText(item.name);
		ImageLoader.getInstance().displayImage(item.icon, image, options, this);
		return convertView;
	}

	public void setAppList(AppList result) {
		mAppList = result;
	}

	@Override
	public void onLoadingCancelled(String arg0, View arg1) {
		mContext.onItemLoaded();
	}

	@Override
	public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
		mContext.onItemLoaded();
	}

	@Override
	public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
		mContext.onItemLoaded();
	}

	@Override
	public void onLoadingStarted(String arg0, View arg1) {
		
	}
	
}
