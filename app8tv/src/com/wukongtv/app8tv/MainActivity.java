package com.wukongtv.app8tv;


import java.io.FileInputStream;
import java.io.InputStream;

import org.json.JSONArray;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;
import com.wukongtv.app8tv.AppGridAdapter.ViewHolder;
import com.wukongtv.app8tv.InstallService.LocalBinder;
import com.wukongtv.app8tv.base.AppList;
import com.wukongtv.app8tv.base.AppList.AppItem;
import com.wukongtv.app8tv.base.HttpJsonClient;
import com.wukongtv.app8tv.base.MemoryClean;
import com.wukongtv.app8tv.base.SmoothProgressBar;
import com.wukongtv.app8tv.base.Utils;
 
public class MainActivity extends FragmentActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, OnClickListener{

	public static final String TAG = "zhangge";
	public static final int START_PROGRESS = 95;
	public static final int ZERO_PROGRESS = 100;
	public static final int CROSSFADE_DURATION = 300;
	public static int SELECT_DURATION = 250;
	public static final long MEMORY_VALUE = 0;
	
    private Handler mHandler;
	private Button mSystemSet;
	private Button mMemoryClean;
	
	private GridView mGridView;
	private AppGridAdapter mGridAdapter;
	private View mContentView;
	private TextView mAppDescription;
	
	private boolean mBound;
	private InstallService mService;
	
	static final int IMAGE_NUM_IN_ONE_SCREEN = 8;
	int mLoadedImageNum;
	
	public static String MSG_MXPLAYER_PKG="com.mxtech.videoplayer.ad";
	public static String MSG_DECODER_PKG="com.mxtech.videoplayer.jm";
	
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			LocalBinder binder = (LocalBinder) service;
			mService = binder.getService();
			mBound = true;
			mService.registerActivity(MainActivity.this);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mHandler = new Handler();
		mSystemSet = (Button)findViewById(R.id.btn_set);
		mSystemSet.setOnClickListener(this);
		mMemoryClean = (Button)findViewById(R.id.claen_btn);
		mMemoryClean.setOnClickListener(this);
		
		UmengUpdateAgent.setUpdateOnlyWifi(false);
		mGridAdapter = new AppGridAdapter(this);
		mGridView = (GridView) findViewById(R.id.app_grid);
		mGridView.setAdapter(mGridAdapter);
		mGridView.setOnItemClickListener(this);
		mGridView.setOnItemSelectedListener(this);
		
		SELECT_DURATION = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
		
		mAppDescription = (TextView) findViewById(R.id.description);
		
		//PauseOnScrollListener listener = new PauseOnScrollListener(ImageLoader.getInstance(), true, true);
		//mGridView.setOnScrollListener(listener);
		
		//mLoadingScreen = (ImageView) findViewById(R.id.app_loading);
		mContentView = findViewById(R.id.real_content);
		mLoadedImageNum = 0;
		
		// start service
		startService(new Intent(this, InstallService.class));
		// bind to it
		bindService(new Intent(this, InstallService.class), mConnection, BIND_AUTO_CREATE);
		
		new AppListLoader().execute("");
		new AppListSyncer().execute("");
	}
	
	@Override
	protected void onDestroy() {
		Log.v("zhangge", "mainactivity destory");
		if (mBound) {
			unbindService(mConnection);
		}
		super.onDestroy();
	}
	

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		// refresh applist status
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				checkAllAppStatus();
			}
		};
		mHandler.postDelayed(runnable, 1000);
		if (mGridAdapter.getCount() > 0) {
			Log.v("zhangge", "onpause");
			// refresh progress bars
			if (mService != null) {
				hideAllProgressBarsAndRefreshTag();
				mService.queryDownloadItems();
				mService.requestStats();
			}
		}
		
	}
	
	private void checkAllAppStatus() {
		for (int i = 0; i < mGridAdapter.getCount(); i++) {
			AppItem item = (AppItem) mGridAdapter.getItem(i);
			if (Utils.isPackageExists(this, item.pkg)) {
				item.status = AppList.STATUS_INSTALLED;
			}
			else {
				item.status = AppList.STATUS_NOT_INSTALLED;
			}
		}
	}

	private void hideAllProgressBarsAndRefreshTag() {
		int first_view = mGridView.getFirstVisiblePosition();
		int last_view = mGridView.getLastVisiblePosition();
		for (int i = first_view; i <= last_view; i++) {
			View view = mGridView.getChildAt(i - first_view);
			AppGridAdapter.ViewHolder vh = (AppGridAdapter.ViewHolder)view.getTag();
			AppItem item = (AppItem) mGridAdapter.getItem(i);
			if(item.status == AppList.STATUS_INSTALLED) {
				//vh.tag.setVisibility(View.INVISIBLE);
				vh.pb.setVisibility(View.INVISIBLE);
			}
			else if (item.status == AppList.STATUS_NOT_INSTALLED) {
				initProgressBar(vh.pb);
				vh.pb.setVisibility(View.VISIBLE);
				//vh.tag.setVisibility(View.VISIBLE);
			}
		}
	}
	
	private void initProgressBar(ProgressBar pb) {
		SmoothProgressBar spb = (SmoothProgressBar) pb;
		spb.init();
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		AppItem item = (AppItem) mGridAdapter.getItem(position);
		if (Utils.isPackageExists(this, item.pkg)) {
			item.status = AppList.STATUS_INSTALLED;
		}
		else {
			item.status = AppList.STATUS_NOT_INSTALLED;
		}
	
		switch(item.status) {
		case AppList.STATUS_DOWNLOADING:
			Toast.makeText(this, getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();
			break;
		case AppList.STATUS_INSTALLED:
			// just open the app 
			Utils.openAppByPackageName(this, item.pkg); 
			MobclickAgent.onEvent(this, getResources().getString(Constants.EVENT_OPEN), item.pkg);
			break;
		case AppList.STATUS_NOT_INSTALLED:
			Toast.makeText(this, getResources().getString(R.string.start_download), Toast.LENGTH_SHORT).show();
			final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
			if (item.apk.startsWith("http")) {
				Request request = new Request(Uri.parse(item.apk));
				//获取当前应用包名进行判断，是否为MXPalyer
				String str = item.pkg;
				if(str.equals(MSG_MXPLAYER_PKG))
				{
					onDecoderDown(position);
				}
				request.setDescription(getResources().getString(R.string.downloading));
				request.setTitle(item.name);
				long download_id = manager.enqueue(request);
				item.status = AppList.STATUS_DOWNLOADING;
				mService.addDownloadWatchItem(download_id, position, String.format("%s.apk", item.pkg));
				MobclickAgent.onEvent(this, getResources().getString(Constants.EVENT_STARTDOWNLOAD), item.pkg);
			}
			break;
		}
		
	}
	
	public void onDecoderDown(int num){
		AppItem item= (AppItem) mGridAdapter.getItem(num);
		final String decode = item.decoder;
		if(!(decode == null || decode.equals("")))
		{
			
			final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
			Request request = new Request(Uri.parse(decode));
			long download_id = manager.enqueue(request);
			//监控进度
			mService.addDownloadWatchItem(download_id,333, String.format("%s.apk",MSG_DECODER_PKG));

		}
		
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		//Log.v("zhangge", "selected");
		//View lastView = mLastItemView.get();
		ViewHolder vh = (ViewHolder) view.getTag();
		final FrameLayout image = vh.frame;
		AnimatorSet set = new AnimatorSet();
		AppItem item = (AppItem) mGridAdapter.getItem(position);
		if (item != null) {
			mAppDescription.setText(item.desc);
		}

		set.play(ObjectAnimator.ofFloat(image, View.SCALE_X, 1.0f, 1.2f))
			.with(ObjectAnimator.ofFloat(image, View.SCALE_Y, 1.0f, 1.2f))
			.before(ObjectAnimator.ofFloat(image, View.SCALE_X, 1.2f, 1.0f))
			.with(ObjectAnimator.ofFloat(image, View.SCALE_Y, 1.2f, 1.0f));
		set.setDuration(SELECT_DURATION);
		set.start();

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			showAboutDialog();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void showAboutDialog() {
		FragmentManager fm = getSupportFragmentManager();
		AboutDialog ad = new AboutDialog();
		ad.show(fm, "about_dlg");
	}
	
	public void onError() {
		// this is a fatal error
		Toast.makeText(this, getResources().getString(R.string.fatal_error), Toast.LENGTH_LONG).show();
	}
	
	public void onItemLoaded() {
		//Log.v("zhangge", String.format("Loaded value: %d", mLoadedImageNum));
		if (mLoadedImageNum != -1) {
			mLoadedImageNum ++;
		}
		if (mLoadedImageNum >= IMAGE_NUM_IN_ONE_SCREEN) {
			crossfade();
			mLoadedImageNum = -1;
		}
	}
	
	private void crossfade() {
		mContentView.setVisibility(View.VISIBLE);
		getWindow().setBackgroundDrawable(null);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
	    // Animate the content view to 100% opacity, and clear any animation
	    // listener set on the view.
//	    mContentView.animate()
//	            .alpha(1f)
//	            .setDuration(CROSSFADE_DURATION)
//	            .setInterpolator(new AccelerateInterpolator())
//	            .setListener(new AnimatorListener() {
//					
//					@Override
//					public void onAnimationStart(Animator animation) {
//					}
//					
//					@Override
//					public void onAnimationRepeat(Animator animation) {
//					}
//					
//					@Override
//					public void onAnimationEnd(Animator animation) {
//						mContentView.setVisibility(View.VISIBLE);
//						getWindow().setBackgroundDrawable(null);
//						getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
//					}
//					
//					@Override
//					public void onAnimationCancel(Animator animation) {
//					}
//				});
		UmengUpdateAgent.setUpdateAutoPopup(true);
		UmengUpdateAgent.update(this);
	    
	}
	
	public void onProgressUpdate(int index, int downloaded_percentage) {
		
		
		int position = index;
		int first_view = mGridView.getFirstVisiblePosition();
		int last_view = mGridView.getLastVisiblePosition();
		//Log.v("zhangge", String.format("on progress update %d : %d, view range %d - %d", index, downloaded_percentage, first_view, last_view));
		if (position <= last_view && position >= first_view) {	
			// update progress bar
			View view = mGridView.getChildAt(position - first_view); 
			if (true) {
				AppGridAdapter.ViewHolder vh = (AppGridAdapter.ViewHolder)view.getTag();
				vh.pb.setVisibility(View.VISIBLE);
				//vh.tag.setVisibility(View.VISIBLE);
				int p = 100 - downloaded_percentage;
				if (p > START_PROGRESS) {
					p = START_PROGRESS;
				}
				SmoothProgressBar spb = (SmoothProgressBar)vh.pb;
				spb.setSmoothProgress(p);
			}
			
		}
		AppItem item = (AppItem) mGridAdapter.getItem(index);
		if (item != null && item.status != AppList.STATUS_DOWNLOADING) {
			item.status = AppList.STATUS_DOWNLOADING;
		}
	}
	
	public void onStats(int event_id, int event_pkg, Long duration) {
		String eventId = getResources().getString(event_id);
		AppItem item = (AppItem)mGridAdapter.getItem(event_pkg);
		if (item != null) {
			String eventPkg = item.pkg;
			if (duration == 0) {
				MobclickAgent.onEvent(this, eventId, eventPkg);
				//Log.v("zhangge", String.format("onStats : %s %s", eventId, eventPkg));
			} 
			else {
				MobclickAgent.onEventDuration(this, eventId, eventPkg, duration);
				//Log.v("zhangge", String.format("onStats : %s %s %d", eventId, eventPkg, duration));
			}
		}
		
	}
	
	public void onComplete(int index) {
		int position = index;
		int first_view = mGridView.getFirstVisiblePosition();
		int last_view = mGridView.getLastVisiblePosition();
		if (position <= last_view && position >= first_view) {
			// update progress bar
			View view = mGridView.getChildAt(position - first_view);
			AppGridAdapter.ViewHolder vh = (AppGridAdapter.ViewHolder)view.getTag();
			SmoothProgressBar spb = (SmoothProgressBar)vh.pb;
			spb.complete();
			//vh.tag.setVisibility(View.INVISIBLE);
		}
		
	}
	

	@Override
	public void onBackPressed() {
		if (Utils.isMyLauncherDefault(this)) {
			MobclickAgent.onEvent(this, getResources().getString(Constants.EVENT_BACK_PRESSED));
			Intent i = new Intent(Intent.ACTION_MAIN);
			i.addCategory(Intent.CATEGORY_HOME);
			i.addCategory(Intent.CATEGORY_DEFAULT);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(Intent.createChooser(i, getResources().getString(R.string.choose_launcher)));
		}
		else {
			super.onBackPressed();
		}
	}
	
	private class AppListLoader extends AsyncTask<String, Integer, AppList> {

		@Override
		protected AppList doInBackground(String... params) {
			boolean found = true;
			JSONArray response = null;
			// find in private files dir
			try {
				FileInputStream ins = MainActivity.this.openFileInput(Constants.APP_LIST_FILENAME);
				String result= HttpJsonClient.convertStreamToString(ins);
				response = new JSONArray(result);
			}
			catch (Exception e) {
				found = false;
			}
			
			// not found, extract from asset
			if (!found) {
				AssetManager assetMgr = getAssets();
				try {
					InputStream ins = assetMgr.open(Constants.APP_LIST_FILENAME);
					String result= HttpJsonClient.convertStreamToString(ins);
					response = new JSONArray(result);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			AppList al = null;
			if (response != null) {
				al = new AppList();
				al.FromJSON(response);		
			}
		
			return al;
		}

		@Override
		protected void onPostExecute(AppList result) {
			super.onPostExecute(result);
			if (result == null) {
				onError();
				return;
			}
			mGridAdapter.setAppList(result);
			mLoadedImageNum = 0;
			checkAllAppStatus();
			if (mService != null) {
				hideAllProgressBarsAndRefreshTag();
				mService.queryDownloadItems();
				mService.requestStats();
			}
			mGridAdapter.notifyDataSetChanged();
			mGridView.requestFocus();
		}

		@Override
		protected void onCancelled(AppList result) {
			super.onCancelled(result);
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
		
	}
	
	private class AppListSyncer extends AsyncTask<String, Integer, Integer> {

		@Override
		protected Integer doInBackground(String... params) {
			HttpJsonClient.requestAndSave(Constants.APP_LIST_URL, Constants.APP_LIST_FILENAME, MainActivity.this);
			return null;
		}
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_set:
			MobclickAgent.onEvent(this, getResources().getString(R.string.E_SystemSetting));
			 try
		        {
		            Intent intent_letv = new Intent();
		            intent_letv = getPackageManager().getLaunchIntentForPackage("com.letv.t1.setting");
		            startActivity(intent_letv);
		            return;
		        }
		        catch(Exception e)
		        {
		            try
		            {
		                Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
		                startActivity(intent);
		                return ;
		            }
		            catch(Exception e1)
		            {
		                Toast.makeText(this,R.string.system_set_error,Toast.LENGTH_LONG).show();
		            }
		        }
			break;

		case R.id.claen_btn:
			MobclickAgent.onEvent(this, getResources().getString(R.string.E_MemoryClean));
			MemoryClean memoryClear = new MemoryClean();
			long availableMemory = memoryClear.getAvailMemory(this);
			memoryClear.clear(this);
			long cleanMemory = memoryClear.getAvailMemory(this);
			long memory = cleanMemory - availableMemory;
			 if(memory>MEMORY_VALUE){
			        Toast.makeText(this,getResources().getString(R.string.ok_memoryClear)+ memory +"M", Toast.LENGTH_LONG).show();
			        }else{
			        	Toast.makeText(this, R.string.already_memoryClear, Toast.LENGTH_LONG).show();
			        
			        }
			break;
		}
		
	}
	
}
