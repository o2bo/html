package com.wukongtv.app8tv.base;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class AppStatusList {
	
	public static final int STATUS_DOWNLOADING = AppList.STATUS_DOWNLOADING;
	public static final int STATUS_DOWNLOAD_COMPLETE = 5;
	
	public class AppItemStatus {
		public int status;
		public int percent_downloaded;
		public long download_id;
		public int item_index; // pivot index to AppList
		public String file_name;
		public long in_time;
		
		
		public AppItemStatus(String fname, int index, int status) {
			item_index = index;
			this.status = status;
			file_name = fname;
			percent_downloaded = 0;
			download_id = 0;
			in_time = System.currentTimeMillis();
		}
	}
	
	private HashMap<Long, AppItemStatus> mDownloadIdItemMap;
	
	public AppStatusList() {
		mDownloadIdItemMap = new HashMap<Long, AppStatusList.AppItemStatus>();
	}
	

	public int getIndexByDownloadid(Long download_id) {
		AppItemStatus item = mDownloadIdItemMap.get(download_id);
		int index = -1;
		if(item != null) {
			index = item.item_index;
		}
		return index;
		
	}
	
	public String getFilenameByDownloadid(Long download_id) {
		AppItemStatus item = mDownloadIdItemMap.get(download_id);
		String filename = "";
		if(item != null) {
			filename = item.file_name;
		}
		return filename;
		
	}
	
	public void addDownloadStatus(Long download_id, int index, String fname) {
		AppItemStatus item = mDownloadIdItemMap.get(download_id);
		if (item == null) {
			item = new AppItemStatus(fname, index, STATUS_DOWNLOADING);
			mDownloadIdItemMap.put(download_id, item);
		}
	}
	
	public void updateDownloadStatus(Long download_id, int status, int percent) {
		AppItemStatus item = mDownloadIdItemMap.get(download_id);
		if (item != null) {
			item.status = status;
			item.percent_downloaded = percent;
			if (status == STATUS_DOWNLOAD_COMPLETE) {
				mDownloadIdItemMap.remove(download_id);
			}
		}
	}
	
	public AppItemStatus completeDownloadStatus(Long download_id) {
		AppItemStatus item = mDownloadIdItemMap.get(download_id);
		if (item != null) {
			item.status = STATUS_DOWNLOAD_COMPLETE;
			return mDownloadIdItemMap.remove(download_id);
		}
		return null;
	}
	
	public int size() {
		return mDownloadIdItemMap.size();
	}
	
	public Set<Long> keySet() {
		return mDownloadIdItemMap.keySet();
	}
	
	public Set<Entry<Long, AppItemStatus>> entrySet() {
		return mDownloadIdItemMap.entrySet();
	}
}
