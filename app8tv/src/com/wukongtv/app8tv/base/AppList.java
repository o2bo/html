package com.wukongtv.app8tv.base;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.wukongtv.app8tv.CpuType;

public class AppList {
	
	public static final int MAX_DESC_LENGTH = 100;
	
	public static final int STATUS_NOT_INSTALLED = 0;
	public static final int STATUS_INSTALLED = 1;
	public static final int STATUS_DOWNLOADING = 2;
	
	public class AppItem {
		public String name;
		public String icon;
		public String apk;
		public String pkg;
		public String desc;
		public String decoder;
		public int size;
		public int status;
		public int index;
		
		public AppItem() {
			name = null;
			icon = null;
			apk = null;
			pkg = null;
			desc = null;
			size = 0;
			status = STATUS_NOT_INSTALLED;
			index = 0;
		}
	}
	
	private ArrayList<AppItem> mAppList;
	
	public AppList() {
		mAppList = new ArrayList<AppList.AppItem>();
	}
	
	public void FromJSON(JSONArray json) {
		if (json == null) {
			return;
		}
		try {
			for (int i = 0; i < json.length(); i++) {
				JSONObject j = json.getJSONObject(i);
				JSONObject k = null;
				AppItem item = new AppItem();
				item.name = j.getString("name");
				item.icon = j.getString("icon");
				item.apk = j.getString("apk");
				item.pkg = j.getString("package");
				item.desc = j.getString("desc");
				if (item.desc.length() > MAX_DESC_LENGTH) {
					item.desc = item.desc.substring(0, MAX_DESC_LENGTH);
				}
				item.size = j.getInt("size");
				item.index = i;
				mAppList.add(item);
				try{
					String cputype="";
					//获取设备cpu型号
					cputype = CpuType.getCpuType();
					k = j.getJSONObject("dpdcy");
					//如果JSONObject k中存在我的cpu
					if(k.has(cputype))
					{
						item.decoder=k.getString(cputype);
					}
					else
					{
						item.decoder="";
					}
				}catch(JSONException e){
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public int getCount() {
		return mAppList.size();
	}
	
	public AppItem get(int index) {
		if(index < mAppList.size()) {
			return mAppList.get(index);
		}
		return null;
	}

}
