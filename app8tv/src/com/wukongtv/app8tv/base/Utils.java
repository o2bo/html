package com.wukongtv.app8tv.base;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Utils {
	
	static final boolean LOG_ENABLED = true;
	
	public static boolean IsInternetAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo net_info = cm.getActiveNetworkInfo();
		if (net_info != null && net_info.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	
	public static <T> void CopyFile(T copyFrom, File copyTo) {
		
	}
	
	public static boolean isPackageExists(Context c, String packageName) {
		final PackageManager pm = c.getPackageManager();
		try {
			pm.getPackageInfo(packageName, PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
			return false;
		}
		return true;
	}
	
	public static void openAppByPackageName(Context c, String packageName) {
		final PackageManager pm = c.getPackageManager();
		Intent i = pm.getLaunchIntentForPackage(packageName);
		if (i != null) {
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(i);
		}
	}
	
	public static boolean isMyLauncherDefault(Context c) {
	    final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
	    filter.addCategory(Intent.CATEGORY_HOME);

	    List<IntentFilter> filters = new ArrayList<IntentFilter>();
	    filters.add(filter);

	    final String myPackageName = c.getPackageName();
	    List<ComponentName> activities = new ArrayList<ComponentName>();
	    final PackageManager packageManager = (PackageManager) c.getPackageManager();

	    // You can use name of your package here as third argument
	    packageManager.getPreferredActivities(filters, activities, null);

	    for (ComponentName activity : activities) {
	        if (myPackageName.equals(activity.getPackageName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public static void log(String fmt, Object... args) {
		if (LOG_ENABLED) {
			Log.i("app8tv", String.format(fmt, args));
		}
	}

}
