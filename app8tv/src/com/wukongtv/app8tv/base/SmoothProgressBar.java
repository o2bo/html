package com.wukongtv.app8tv.base;

import java.util.LinkedList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

/*
 * notice: this is not like ProgressBar, it's not thread-safe
 * you must ensure all calls are from UI thread
 */
public class SmoothProgressBar extends ProgressBar {
	
	private static final int STEP_MILISECONDS = 20;
	
	private int mCurrentTargetProgress;
	
	private LinkedList<Integer> mSmoothProgressInts = new LinkedList<Integer>();
	
	private boolean mRunning = false;
	
	private Runnable mSmoothRunnable = new Runnable() {
		
		@Override
		public void run() {
			if (mSmoothProgressInts.size() > 0) {
				int progress = mSmoothProgressInts.pop();
				setProgress(progress);
			}
		}
	};

	public SmoothProgressBar(Context context) {
		super(context);
		mCurrentTargetProgress = 100;
	}

	public SmoothProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		mCurrentTargetProgress = 100;
	}

	public SmoothProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mCurrentTargetProgress = 100;
	}
	
	public void init() {
		if (!mRunning) {
			mCurrentTargetProgress = 100;
			setProgress(100);
			mSmoothProgressInts.clear();	
		}
	}
	
	public void complete() {
		mRunning = false;
		this.setVisibility(View.INVISIBLE);
	}
	

	@Override
	protected void onDetachedFromWindow() {
		if (mSmoothRunnable != null) {
			removeCallbacks(mSmoothRunnable);
		}
		super.onDetachedFromWindow();
	}

	public synchronized void setSmoothProgress(int progress) {
		if (!mRunning) {
			mRunning = true;
		}
		if (mCurrentTargetProgress == progress) {
			return;
		}
		
		boolean inverse = (progress < mCurrentTargetProgress);
		if (inverse) {
			int steps = mCurrentTargetProgress - progress;
			for (int i = 0; i < steps; i++) {
				mSmoothProgressInts.add(mCurrentTargetProgress - 1 - i);
				postDelayed(mSmoothRunnable, STEP_MILISECONDS * i);
			}
		}
		else {
			int steps = progress - mCurrentTargetProgress ;
			for (int i = 0; i < steps; i++) {
				mSmoothProgressInts.add(mCurrentTargetProgress + i + 1);
				postDelayed(mSmoothRunnable, STEP_MILISECONDS * i);
			}
		}
		mCurrentTargetProgress = progress;
	}

}
