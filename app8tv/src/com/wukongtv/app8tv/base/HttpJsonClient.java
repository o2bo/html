package com.wukongtv.app8tv.base;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.util.Log;

public class HttpJsonClient {
	
	static final int URL_CONNECTION_TIMEOUT = 8000;
	static final int URL_CONNECTION_READ_TIMEOUT = 30000;
	
	public static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/* 
	 * this is a simple json request capsulation
	 * return the json object or null
	 */
	public static <T> T request(String url, Class<T> type)
	{
		T json = null;
		HttpClient httpclient = new DefaultHttpClient();
		final HttpParams http_params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(http_params, URL_CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(http_params, URL_CONNECTION_READ_TIMEOUT);

		// Prepare a request object
		HttpGet httpget = new HttpGet(url); 

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			// Examine the response status
			Log.i("Praeda",response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= convertStreamToString(instream);
				Log.i("Praeda",result);

				// A Simple JSONObject Creation
				json = type.getConstructor(String.class).newInstance(result);

				// Closing the input stream will trigger connection release
				instream.close();
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public static void requestAndSave(String url, String filename, Context context) {
		HttpClient httpclient = new DefaultHttpClient();
		final HttpParams http_params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(http_params, URL_CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(http_params, URL_CONNECTION_READ_TIMEOUT);

		// Prepare a request object
		HttpGet httpget = new HttpGet(url); 

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {
				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				FileOutputStream ofile = context.openFileOutput(filename, 0);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = instream.read(buffer)) != -1) {
				    ofile.write(buffer, 0, len);
				}
				instream.close();
				ofile.close();
			}

			
		} catch (Exception e) {
		}
	}


}
