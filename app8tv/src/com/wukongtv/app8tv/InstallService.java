package com.wukongtv.app8tv;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.NotificationCompat.Builder;
import android.widget.Toast;

import com.wukongtv.app8tv.base.AppStatusList;
import com.wukongtv.app8tv.base.OmniStorage;

public class InstallService extends Service {
	public static final String TAG = "zhangge";

	// UI thread to worker thread
	public static final int MSG_DOWNLOAD_COMPLETE = 917;
	public static final int MSG_DOWNLOAD_STATUS_CHECK = 918;
	public static final int MSG_DOWNLOAD_QUERY = 919;
	public static final int MSG_DOWNLOAD_ADD = 920;
	public static final int MSG_REQUEST_STATS = 921;

	// worker thread to UI thread
	public static final int MSG_UPDATE = 0x819;
	public static final int MSG_COMPLETE = 0x820;
	public static final int MSG_STATS = 0x821;

	// for downloadmanger bug
	public static final int DEFAULT_PAKCAGE_SIZE = 15000000;

	private WeakReference<MainActivity> mWeakActivity;
	private AppStatusList mDownloadingApps;
	private StatisticsHelper mStatHelper = new StatisticsHelper();
	private Notification mNoti;
	
	public static String MSG_MXPLAYER_FILENAME="com.mxtech.videoplayer.ad.apk";
	public static String MSG_DECODER_FILENAME="com.mxtech.videoplayer.jm.apk";
	long mMxplayerPercentage = 0;
	long mMxplayerPercentageTrue = 0;
	long mDecoderPercentage = 0;
	int mMxplayerIndex=0;
	int mDecoderIndex=0;
	long mDecoderStatus = 0;
	long mMxplayerDownloadid;
	long mDecoderDownloadid;

	private BroadcastReceiver mDownloadCompleteReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// just install the app
			long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
			//Log.v(TAG, String.format("receive :%d", downloadId));
			//AppStatusList.AppItemStatus item = mDownloadingApps.completeDownloadStatus(downloadId);
			Message msg = new Message();
			msg.what = MSG_DOWNLOAD_COMPLETE;
			msg.arg1 = (int) downloadId;
			mServiceHandler.handleMessage(msg);
		}
	};

	public class LocalBinder extends Binder {
		InstallService getService() {
			return InstallService.this;
		}
	}

	/*
	 * code should run in service thread
	 * should call ServiceHandler.sendMsg
	 */
	private final class ServiceHandler extends Handler {
		private static final int TIMER_INTERVAL = 500;
		private Runnable mTimerRunnable;


		public ServiceHandler(Looper l) {
			super(l);
			mTimerRunnable = new Runnable() {
				@Override
				public void run() {
					sendEmptyMessage(MSG_DOWNLOAD_STATUS_CHECK);
				}
			};
			this.postDelayed(mTimerRunnable, TIMER_INTERVAL);
		}
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case MSG_DOWNLOAD_COMPLETE:
				long download_id = msg.arg1;
				final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
				DownloadManager.Query q = new DownloadManager.Query();
				q.setFilterById(download_id);
				Cursor cursor = manager.query(q);

				if (cursor.moveToFirst()) {
					int currentStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)); 
					cursor.close();
					mDownloadingApps.updateDownloadStatus(download_id, AppStatusList.STATUS_DOWNLOADING, 100);
					if ( currentStatus != DownloadManager.STATUS_SUCCESSFUL) {
						// download complete, but not successful, then it is a error
						Toast.makeText(InstallService.this, getResources().getString(R.string.download_error), Toast.LENGTH_SHORT).show();
						return;
					}
					AppStatusList.AppItemStatus item = mDownloadingApps.completeDownloadStatus(download_id);
					if (item != null) {
						long now = System.currentTimeMillis();
						mStatHelper.setItem(R.string.E_DownloadingDuration, item.item_index, now - item.in_time);
						String compareFilename;
						compareFilename=item.file_name;
						if(compareFilename.equals(MSG_MXPLAYER_FILENAME))
						{
							mMxplayerPercentageTrue = 100;
							if(mDecoderPercentage==100)
							{
								afterDownload(download_id, item.file_name, item.item_index);
								afterDownload(mDecoderDownloadid, MSG_DECODER_FILENAME, mDecoderIndex);
							}
							else if(mDecoderStatus==0)
							{
								afterDownload(download_id, item.file_name, item.item_index);
							}
						}
						else if(compareFilename.equals(MSG_DECODER_FILENAME))
						{
							mDecoderPercentage = 100;
							if(mMxplayerPercentageTrue == 100)
							{
								afterDownload(mMxplayerDownloadid, MSG_MXPLAYER_FILENAME, mMxplayerIndex);
								afterDownload(download_id, item.file_name, item.item_index);
							}
							
						}
						else
						{
							afterDownload(download_id, item.file_name, item.item_index);
						}
					}
				}
				break;

			case MSG_DOWNLOAD_STATUS_CHECK:
				checkDownloadProgress();
				this.postDelayed(mTimerRunnable, TIMER_INTERVAL);
				break;
			case MSG_DOWNLOAD_QUERY:
				sendDownloadStatus();
				break;
			case MSG_DOWNLOAD_ADD:

				mDownloadingApps.addDownloadStatus(Long.valueOf(msg.arg2), msg.arg1, (String) msg.obj);
				break;
			case MSG_REQUEST_STATS:
				sendStats();
				break;
			default:
				break;
			}
		}
	}

	/*
	 * code should run in main(UI) thread
	 * should call UIHandler.sendMsg
	 */
	private final class UIHandler extends Handler {
		public UIHandler(Looper l) {
			super(l);
		}

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			MainActivity ma = mWeakActivity.get();
			if (ma == null) {
				return;
			}
			switch (msg.what) {
			case MSG_COMPLETE:
				if (msg.arg2 <= 0) {
					stopForeground(true);
				}
				ma.onComplete(msg.arg1);
				break;

			case MSG_UPDATE:
				ma.onProgressUpdate(msg.arg1, msg.arg2);
				break;

			case MSG_STATS:
				ma.onStats(msg.arg1, msg.arg2, (Long)msg.obj);
				break;
			default:
				break;
			}
		}

	}

	private class StatisticsItem {
		public int event_id;
		public int event_pkg;
		public long event_time;
		public StatisticsItem (int id, int pkg, long time) {
			event_id = id;
			event_pkg = pkg;
			event_time = time;
		}
	}
	private class StatisticsHelper {
		LinkedList<StatisticsItem> mStatisticsItems = new LinkedList<InstallService.StatisticsItem>();
		public void setItem(int event_id, int pkg, long time) {
			mStatisticsItems.add(new StatisticsItem(event_id, pkg, time));
		}
		public void setItem(int event_id, int pkg) {
			setItem(event_id, pkg, 0);
		}
		public StatisticsItem getItem(int index) {
			return mStatisticsItems.get(index);
		}
		public int size() {
			return  mStatisticsItems.size();
		}
		public void clear() {
			mStatisticsItems.clear();
		}
	}

	private final IBinder mBinder = new LocalBinder();

	private volatile Looper mServiceLooper;
	private volatile ServiceHandler mServiceHandler;
	private volatile UIHandler mUIHandler;
	private String mName;



	/*
	 * here's interfaces for activity
	 * for inter-thread communication
	 */
	public void sendMessageToService(Message msg) {
		mServiceHandler.sendMessage(msg);
	}

	public void registerActivity(MainActivity activity) {
		mWeakActivity = new WeakReference<MainActivity>(activity);
		mUIHandler = new UIHandler(activity.getMainLooper());
	}

	@Override
	public IBinder onBind(Intent intent) {
		//Log.v("zhangge", "service binded");
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		//Log.v("zhangge", "service created");
		HandlerThread thread = new HandlerThread("IntentService[" + mName + "]");
		thread.start();
		mDownloadingApps = new AppStatusList();
		IntentFilter f = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		registerReceiver(mDownloadCompleteReceiver, f);
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		mNoti = new Builder(this)
		.setContentText(getResources().getString(R.string.is_running))
		//.setSmallIcon(R.drawable.ic_launcher)
		.build();

	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Log.v("zhangge", "service started");
		return super.onStartCommand(intent, flags, startId);
	}


	@Override
	public void onLowMemory() {
		super.onLowMemory();
		//Log.w("zhangge", "ohhh im low memo");
	}

	@Override
	public void onDestroy() {
		unregisterReceiver(mDownloadCompleteReceiver);
		//Log.w("zhangge", "ohhh im dying");
		mServiceLooper.quit();
		super.onDestroy();
	}

	//////////////// METHOD BELOW SHOULD BE CALLED FROM WORKER THREAD ///////////////////////


	private void afterDownload(long downloadId, String filename, int index) {
		final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		File install_apk = null;
		//Log.v(TAG, String.format("download id: %d", downloadId));

		Uri downloaded_uri = manager.getUriForDownloadedFile(downloadId);
		if (!trySilentInstall(downloaded_uri)) {
			// not privileged for install, try use normal method
			try {
				ParcelFileDescriptor downloadedFile = manager.openDownloadedFile(downloadId);
				FileDescriptor fd = downloadedFile.getFileDescriptor();
				FileInputStream fin = new FileInputStream(fd);
				OmniStorage storage = ((MyApplication)getApplication()).getOmniStorage();

				FileOutputStream fout = storage.getPublicFileOutputStream(storage.getPublicStorage(null), filename);
				byte []buf = new byte[1024];
				while (fin.read(buf) > 0) {
					fout.write(buf);
				}
				fin.close();
				fout.close();
				downloadedFile.close();
				install_apk = new File(storage.getPublicStorage(null), filename);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// now install
			if (install_apk != null) {
				mStatHelper.setItem(R.string.E_InstallBegin, index);
				Intent promptInstall = new Intent(Intent.ACTION_VIEW);
				promptInstall.setDataAndType(Uri.fromFile(install_apk), "application/vnd.android.package-archive");
				promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(promptInstall);

			}
		}

		// notify UI thread install done
		Message msg = new Message();
		msg.what = MSG_COMPLETE;
		msg.arg2 = mDownloadingApps.size();
		msg.arg1 = index;
		mUIHandler.sendMessage(msg);
	}

	private boolean trySilentInstall(Uri apk) {
		return false;
		//		try {
		//			ApplicationManager am = new ApplicationManager(this);	
		//			am.setOnInstalledPackaged(new ApplicationManager.OnInstalledPackaged() {
		//				
		//				@Override
		//				public void packageInstalled(String packageName, int returnCode) {
		//					Toast.makeText(InstallService.this, "install success",  Toast.LENGTH_LONG);
		//				}
		//			});
		//			am.installPackage(apk);
		//		}
		//		catch (Exception e) {
		//			return false;
		//		}
		//		return true;
	}

	private void checkDownloadProgress() {
		// doin' work!!
		final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		if (mDownloadingApps.size() > 0) {
			DownloadManager.Query q = new DownloadManager.Query();
			long [] ids = new long[mDownloadingApps.size()];
			int i = 0;
			for (Long l : mDownloadingApps.keySet()) {
				ids[i] = l;
				i++;
			}
			q.setFilterById(ids);
			Cursor cursor = manager.query(q);
			Set<Long> result_ids = new TreeSet<Long>();
			if (cursor.moveToFirst()) {
				do {
					long downloadId = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_ID));
					result_ids.add(downloadId);
					int currentStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)); 
					if ( currentStatus == DownloadManager.STATUS_RUNNING) {
						long bytesDownloaded = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
						long bytesAll = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
						if (bytesAll == -1) {
							bytesAll = DEFAULT_PAKCAGE_SIZE;
						}
						//�ж��Ƿ�ΪMXplayer
						String compareFilename = mDownloadingApps.getFilenameByDownloadid(downloadId);
						if(compareFilename.equals(MSG_MXPLAYER_FILENAME))
						{
							mMxplayerPercentageTrue = bytesDownloaded * 100 / bytesAll;
							mMxplayerPercentage = bytesDownloaded * 100 / bytesAll;
							mMxplayerDownloadid = downloadId;
							mMxplayerIndex = mDownloadingApps.getIndexByDownloadid(downloadId);
							if(mMxplayerPercentage > mDecoderPercentage && mDecoderPercentage == 1)
							{
								mMxplayerPercentage = mDecoderPercentage;
							}
							mDownloadingApps.updateDownloadStatus(downloadId, AppStatusList.STATUS_DOWNLOADING, (int)mMxplayerPercentage);
						}
						//�ж��Ƿ�ΪMXPlay������
						else if(compareFilename.equals(MSG_DECODER_FILENAME))
						{
							mDecoderIndex = 333;
							mDecoderStatus = 1;
							mDecoderDownloadid = downloadId;
							mDecoderPercentage = bytesDownloaded * 100 / bytesAll;
						}
						else{
							long percentage = bytesDownloaded * 100 / bytesAll;
							//Log.v("zhangge", String.format("bytes downloaded: %s bytesAll: %s percentage : %d", String.valueOf(bytesDownloaded), String.valueOf(bytesAll), percentage));
							mDownloadingApps.updateDownloadStatus(downloadId, AppStatusList.STATUS_DOWNLOADING, (int)percentage);
						}
					}
					else if(currentStatus == DownloadManager.STATUS_SUCCESSFUL) {
						// bravo bravo! 
						//Log.v("zhangge", "download success");
						String compareFilename;
						compareFilename = mDownloadingApps.getFilenameByDownloadid(downloadId);
						if(compareFilename == MSG_MXPLAYER_FILENAME)
						{
							mMxplayerPercentageTrue=100;
						}
						else if(compareFilename == MSG_DECODER_FILENAME)
						{
							mDecoderPercentage=100;
						}
					}
					else if (currentStatus == DownloadManager.STATUS_FAILED) {
						AppStatusList.AppItemStatus item = mDownloadingApps.completeDownloadStatus(downloadId);
						mStatHelper.setItem(R.string.E_DownloadFailure, item.item_index);
						//Log.v("zhangge", "download fail");
					}
					else if (currentStatus == DownloadManager.STATUS_PAUSED || currentStatus == DownloadManager.STATUS_PENDING) {

					}
					else {
						AppStatusList.AppItemStatus item = mDownloadingApps.completeDownloadStatus(downloadId);
						mStatHelper.setItem(R.string.E_DownloadFailure, item.item_index);
						//Log.v("zhangge", "download fail for unknow reason");
					}
				}
				while(cursor.moveToNext());
				// notify UI thread to update progress bar
				sendDownloadStatus(); 
			}

			// in case the download task is deleted by user
			// we need to check whether all ids exist
			for (int j = 0; j < ids.length; j++) {
				if (!result_ids.contains(ids[j])) {
					mDownloadingApps.completeDownloadStatus(ids[j]);
				}
			}

			cursor.close();
		}
	}

	private void sendDownloadStatus() {
		for (Entry<Long, AppStatusList.AppItemStatus> entry: mDownloadingApps.entrySet()) {
			Message msg = new Message();
			msg.what = MSG_UPDATE;
			msg.arg1 = entry.getValue().item_index;
			msg.arg2 = (int) entry.getValue().percent_downloaded;
			mUIHandler.sendMessage(msg);
		}
	}

	private void sendStats() {
		for (int i = 0; i < mStatHelper.size(); i++) {
			Message smsg = new Message();
			StatisticsItem sitem = mStatHelper.getItem(i);
			smsg.arg1 = sitem.event_id;
			smsg.arg2 = sitem.event_pkg;
			smsg.what = MSG_STATS;
			smsg.obj = Long.valueOf(sitem.event_time);
			mUIHandler.sendMessage(smsg);
		}
		mStatHelper.clear();
	}


	////////////// METHOD BELOW SHOULD BE CALL FROM UI THREAD   /////////////////////////////////////


	public void addDownloadWatchItem(long downloadId, int index, String filename) {
		Message msg = new Message();
		msg.what = MSG_DOWNLOAD_ADD;
		msg.arg1 = index;
		msg.arg2 = (int) downloadId;
		msg.obj = new String(filename);
		startForeground(0x819, mNoti);
		mServiceHandler.sendMessage(msg);
	}

	public void queryDownloadItems() {
		mServiceHandler.sendEmptyMessage(MSG_DOWNLOAD_QUERY);
	}

	public void requestStats() {
		mServiceHandler.sendEmptyMessage(MSG_REQUEST_STATS);
	}
}
